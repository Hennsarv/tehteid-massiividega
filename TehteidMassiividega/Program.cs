﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TehteidMassiividega
{
    class Inimesed
    {
        public string Linn; // { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }

        public override string ToString()
        {
            return $"{Nimi} elab linnas {Linn} ja on {Vanus}-aastane";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Inimesed> Rahvas = new List<Inimesed>
            {
                new Inimesed {Nimi = "Henn", Linn = "Tallinn", Vanus = 62},
                new Inimesed {Nimi = "Henn", Linn = "Tallinn", Vanus = 12},
                new Inimesed {Nimi = "Ants", Linn = "Tallinn", Vanus = 40},
                new Inimesed {Nimi = "Peeter", Linn = "Tallinn", Vanus = 70},
                new Inimesed {Nimi = "Pille", Linn = "Tartu", Vanus = 30},
                new Inimesed {Nimi = "Malle", Linn = "Tartu", Vanus = 25},
                new Inimesed {Nimi = "Ülle", Linn = "Pärnu", Vanus = 20},

            };

            foreach (var x in Rahvas.OrderBy(y => y.Nimi   ).Reverse()) Console.WriteLine(x);

            var LinnaRahvas = Rahvas.ToLookup(x => x.Linn, x => x);

            //foreach (var x in LinnaRahvas.Select(y => y) Console.WriteLine($"Linnas {x.Key} elab {x.Count()} rahvast neis { x.Distinct().Count() } erinevaid nimesid");

            Console.Write("Tartus elavad: ");
            foreach (var x in LinnaRahvas["Tartu"]) Console.Write(x.Nimi + " ");
            Console.WriteLine();
            Console.Write("Tallinnas elavad: ");
            foreach (var x in LinnaRahvas["Tallinn"]) Console.Write(x.Nimi + " ");
            Console.WriteLine();

            int[] arvud = { 1, 2, 3, 4, 3, 5, 6, 2, 8, 1, 9 };
            var grupid = arvud.ToLookup(x => x % 2==0 ? "paaris" : "Paaritu", x => x);



            var qr = from x in arvud
                     orderby x
                     select new { arv = x, ruut = x * x };
            foreach (var x in qr) Console.WriteLine(x);
            


            foreach (var x in grupid["paaris"].OrderBy(x => x)) Console.WriteLine(x);

            var q = from x in Rahvas
                    group x by x.Linn into Mitu
                    select new { Linn = Mitu.Key, Arv = Mitu.Count() };
            foreach (var x in q) Console.WriteLine(x);
                    

        }


    }
}
